package co.mobilemakers.madlibs;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


public class Results_Activity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results_);

        String parameter[] = getIntent().getStringArrayExtra(Intent.EXTRA_TEXT);
        String Story = String.format(" Once upon a time, [%s] --- - -- [%s] ----. " +
                        "-- -------- -- - [%s] -- ---- [%s] -- --- ------ -- [%s]." +
                        "[%s] ----- -- -- [%s] -- ----- [%s] --- -- [%s] --- [%s].",
                parameter[0],parameter[1],
                parameter[2],parameter[3],
                parameter[4],parameter[5],
                parameter[6],parameter[7],
                parameter[8],parameter[9]);

        TextView textViewBody = (TextView)findViewById(R.id.textViewBody);
        textViewBody.setText(Story);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_results_, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
