package co.mobilemakers.madlibs;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class MainActivity extends ActionBarActivity {

    EditText mEditText1, mEditText2,
             mEditText3, mEditText4,
             mEditText5, mEditText6,
             mEditText7, mEditText8,
             mEditText9, mEditText10;

    Button mButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        mEditText1  = (EditText)findViewById(R.id.editText);
        mEditText2  = (EditText)findViewById(R.id.editText2);
        mEditText3  = (EditText)findViewById(R.id.editText3);
        mEditText4  = (EditText)findViewById(R.id.editText4);
        mEditText5  = (EditText)findViewById(R.id.editText5);
        mEditText6  = (EditText)findViewById(R.id.editText6);
        mEditText7  = (EditText)findViewById(R.id.editText7);
        mEditText8  = (EditText)findViewById(R.id.editText8);
        mEditText9  = (EditText)findViewById(R.id.editText9);
        mEditText10 = (EditText)findViewById(R.id.editText10);

        mButton = (Button) findViewById(R.id.button);

        setListeners();

    }

    private void setListeners(){

        TextWatcher TxtWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                Boolean isPopulated = !TextUtils.isEmpty(mEditText1.getText()) && !TextUtils.isEmpty(mEditText2.getText()) &&
                                      !TextUtils.isEmpty(mEditText3.getText()) && !TextUtils.isEmpty(mEditText4.getText()) &&
                                      !TextUtils.isEmpty(mEditText5.getText()) && !TextUtils.isEmpty(mEditText6.getText()) &&
                                      !TextUtils.isEmpty(mEditText7.getText()) && !TextUtils.isEmpty(mEditText8.getText()) &&
                                      !TextUtils.isEmpty(mEditText9.getText()) && !TextUtils.isEmpty(mEditText10.getText());

                mButton.setEnabled(isPopulated);
            }
        };

        mEditText1.addTextChangedListener(TxtWatcher);
        mEditText2.addTextChangedListener(TxtWatcher);
        mEditText3.addTextChangedListener(TxtWatcher);
        mEditText4.addTextChangedListener(TxtWatcher);
        mEditText5.addTextChangedListener(TxtWatcher);
        mEditText6.addTextChangedListener(TxtWatcher);
        mEditText7.addTextChangedListener(TxtWatcher);
        mEditText8.addTextChangedListener(TxtWatcher);
        mEditText9.addTextChangedListener(TxtWatcher);
       mEditText10.addTextChangedListener(TxtWatcher);

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, Results_Activity.class);

                String Result[] = {mEditText1.getText().toString(),
                                   mEditText2.getText().toString(),
                                   mEditText3.getText().toString(),
                                   mEditText4.getText().toString(),
                                   mEditText5.getText().toString(),
                                   mEditText6.getText().toString(),
                                   mEditText7.getText().toString(),
                                   mEditText8.getText().toString(),
                                   mEditText9.getText().toString(),
                                   mEditText10.getText().toString()};

                intent.putExtra(Intent.EXTRA_TEXT, Result);
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
